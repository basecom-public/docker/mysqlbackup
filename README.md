# mysqlbackup

## working and tested features

Docker mysqlbackup image which supports

- mysql restore from S3 and filesystem
- mysql backup to S3 and filesystem

## TODOS

- Choose compression
- schedule regularly backups (currently only on docker run, current only suitable for kubernetes cronjob)
- make it possible to run own scripts for extending this image (example execute SQL queries before/after restore)

# Environment variables

## Core

| Variable                      | Values / Description                  |
| ----------------------------- | ------------------------------------- |
| MODE                          | RESTORE, BACKUP                       |
| DB_USER                       | Which user for backup / restore       |
| DB_PASSWORD                   | Password of DB_USER                   |
| DB_NAME                       | Which database to backup / restore    |
| DB_HOST                       |                                       |
| DB_PORT                       | If not set defaults to 3306           |
| CRONJOB (not implemented yet) | 15min, daily, hourly, monthly, weekly |

## S3

| Variable              | Values |
| --------------------- | ------ |
| AWS_ACCESS_KEY_ID     |        |
| AWS_SECRET_ACCESS_KEY |        |
| AWS_DEFAULT_REGION    |        |
| AWS_ENDPOINT_URL      |        |

## Backup

| Variable         | Values / Description                                                                                        | Default Value |
| ---------------- | ----------------------------------------------------------------------------------------------------------- | ------------- |
| BACKUP_URL       | URL in the formats s3://bucket-name/path-to/dump.sql.gz (with filename) or /backups (local filesystem path) | /bsc/backups/ |
| BACKUP_RETENTION | In days, how long backups should be kept (works only on local filesystem!)                                  | 30            |

## Restore

| Variable    | Values                                                                                                     |
| ----------- | ---------------------------------------------------------------------------------------------------------- |
| RESTORE_URL | URL in the formats s3://bucket-name/path-to/dump.sql.gz or filesystem /bsc/backups/wordpress/latest.sql.gz |

## Development

| Variable | Values      | Description                                                                    |
| -------- | ----------- | ------------------------------------------------------------------------------ |
| DEBUG    | false, true | If set true, shows more information and dont stop container if a command fails |
