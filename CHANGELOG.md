# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.0] - 2022-11-03

- tested mysql backup to S3 and filesystem

## [0.1.1] - 2022-11-03

- tested mysql restore from S3 and filesystem

## [0.1.0] - 2022-11-11-03

- first git tag release
