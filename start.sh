#!/bin/sh

if [ "$DEBUG" != "true" ]; then
  set -e
fi

DUMP_URL="/bsc/backups/"
DB_PORT="3306"
FILENAME=$(date +"%Y-%m-%d-%H-%M")
DUMP_URL_TMP=/bsc/tmp/"$FILENAME"
BACKUP_RETENTION="30"

if [ "$MODE" = "RESTORE" ]; then

  echo "Delete $DB_NAME database for clean restore"
  mysql -h"$DB_HOST" -u"$DB_USER" -p"$DB_PASSWORD" -e "DROP DATABASE IF EXISTS $DB_NAME"
  mysql -h"$DB_HOST" -u"$DB_USER" -p"$DB_PASSWORD" -e "create database $DB_NAME"

  if [[ "$RESTORE_URL" = "s3://"* ]]; then
    echo "S3 Download file"
    aws s3 cp "$RESTORE_URL" "/bsc/tmp/" --endpoint-url "$AWS_ENDPOINT_URL"
    echo "Download step done"

    base_name=$(basename "$RESTORE_URL")
    echo "Start unzip"
    gunzip /bsc/tmp/"$base_name"

    echo "Start restoring"
    mysql -h"$DB_HOST" -u"$DB_USER" -p"$DB_PASSWORD" "$DB_NAME" </bsc/tmp/"${base_name%.*}"
    echo "Done"

  else
    echo "Proceed with local filesystem restore"
    gunzip "$RESTORE_URL"

    echo "Start restoring"
    mysql -h"$DB_HOST" -u"$DB_USER" -p"$DB_PASSWORD" "$DB_NAME" <"${RESTORE_URL%.*}"
    echo "Done"
  fi
fi

if [ "$MODE" = "BACKUP" ]; then
  mysqldump \
    --host="$DB_HOST" --port="$DB_PORT" --user="$DB_USER" --password="$DB_PASSWORD" \
    --lock-tables \
    --no-tablespaces \
    --databases "$DB_NAME" \
    >"$DUMP_URL_TMP.sql"

  gzip -9 "$DUMP_URL_TMP.sql"

  if [[ "$BACKUP_URL" = "s3://"* ]]; then
    echo "Upload sql file to S3"
    aws s3 cp "$DUMP_URL_TMP.sql.gz" "$BACKUP_URL" --endpoint-url "$AWS_ENDPOINT_URL"
    echo "Upload step done"

  else
    echo "Proceed with backup to local filesystem"

    mkdir -p "$DUMP_URL/$DB_NAME/"
    mv "$DUMP_URL_TMP".sql.gz "$DUMP_URL/$DB_NAME/"
    find "$DUMP_URL/$DB_NAME/" -mtime +"$BACKUP_RETENTION" -exec rm {} \;
  fi

fi

# if [ -n "$CRON" ] then
#  # https://devopsheaven.com/cron/docker/alpine/linux/2017/10/30/run-cron-docker-alpine.html
# fi

if [ "$DEBUG" = "true" ]; then
  echo "DEBUG mode is enabled"
  sleep 3600
fi
