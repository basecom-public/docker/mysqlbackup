FROM alpine:3

RUN apk add --no-cache aws-cli mysql-client

RUN addgroup -g 1000 application && \
  adduser --disabled-password --gecos "" --ingroup "application" --no-create-home --uid "1000" "application"

RUN mkdir -p /bsc/tmp/download/ && \
  chown application:application -R /bsc/

ADD --chown=application:application start.sh /bsc/start.sh
WORKDIR /bsc/tmp/


USER application
ENTRYPOINT ["/bsc/start.sh"]
